# adblock-capitalism

This is a [list](https://0xacab.org/cinereal/adblock-capitalism/-/raw/no-masters/lists/capitalism-list.txt) to block .mil and .com/.co domains.
You can use it with [uBlock Origin](https://github.com/gorhill/uBlock). 

## maintaining

- install [Perl](https://www.perl.org)
- install dependency using `sudo cpan Path::Tiny`
- sort and update checksum by running `sh prepare.sh`
- find bad TLDs like:
```
wget https://raw.githubusercontent.com/gavingmiller/second-level-domains/master/SLDs.csv
cat SLDs.csv | awk -F ',' '{print $NF}' | grep '\.mil\.'
cat SLDs.csv | awk -F ',' '{print $NF}' | grep '\.com\.' 
cat SLDs.csv | awk -F ',' '{print $NF}' | grep '\.co\.' 
```
